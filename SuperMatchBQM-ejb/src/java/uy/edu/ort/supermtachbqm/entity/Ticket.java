package uy.edu.ort.supermtachbqm.entity;

public class Ticket {
    
    private Long number;
    private User user;

    public Ticket(Long number, User user) {
        this.number = number;
        this.user = user;
    }

    public Long getNumber() {
        return number;
    }
    
     public User getUser() {
        return user;
    }
    
}

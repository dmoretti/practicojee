package uy.edu.ort.supermtachbqm.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import uy.edu.ort.supermtachbqm.dto.MatchDTO;


@Entity
@Table(name = "partidos")
public class Match implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String nmLocalTeam;
    
    private String nmVisitorTeam;

    public Match() {
    }
    
    public Match(MatchDTO matchDTO) {
        this.nmLocalTeam = matchDTO.getNmLocalTeam();
        this.nmVisitorTeam = matchDTO.getNmVisitorTeam();                
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNmLocalTeam() {
        return nmLocalTeam;
    }

    public void setNmLocalTeam(String nmLocalTeam) {
        this.nmLocalTeam = nmLocalTeam;
    }

    public String getNmVisitorTeam() {
        return nmVisitorTeam;
    }

    public void setNmVisitorTeam(String nmVisitorTeam) {
        this.nmVisitorTeam = nmVisitorTeam;
    }
    
}

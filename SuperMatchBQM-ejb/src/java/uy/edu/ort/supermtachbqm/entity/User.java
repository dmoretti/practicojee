package uy.edu.ort.supermtachbqm.entity;

import java.util.ArrayList;
import java.util.List;

public class User {
    
    private String userName;
    private List<Ticket> lstTickets = 
            new ArrayList<Ticket>();

    public User(String userName) {
        this.userName = userName;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Ticket> getLstTickets() {
        return lstTickets;
    }

    public void addTicket(Ticket ticket){
        this.lstTickets.add(ticket);
    }
    
    
    
}

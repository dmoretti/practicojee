package uy.edu.ort.supermtachbqm.match.service;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.InitialContext;
import uy.edu.ort.supermtachbqm.dto.MatchDTO;

/**
 *
 * @author damianmoretti
 */
@Stateless
public class MatchMessageBean implements MatchMessageBeanLocal {
    
    @Resource(mappedName="jms/ConnectionFactory")
    private ConnectionFactory connectionFactory;
    
    @Resource(mappedName="jms/Topic")
    private Topic topic;
    
   
    public void notifySuscribers(MatchDTO matchDTO){
       
        // Estudiantes: Mejorar manejo de Try - Catch
        try {
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            TextMessage message = session.createTextMessage();
            message.setText(matchDTO.toString());

            MessageProducer messageProducer = session.createProducer(topic);
            messageProducer.send(message);

        } catch (Exception ex) {
            Logger.getLogger(MatchMessageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

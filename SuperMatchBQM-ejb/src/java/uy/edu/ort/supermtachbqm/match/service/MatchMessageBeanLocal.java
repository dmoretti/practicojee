package uy.edu.ort.supermtachbqm.match.service;

import javax.ejb.Local;
import uy.edu.ort.supermtachbqm.dto.MatchDTO;

/**
 *
 * @author damianmoretti
 */
@Local
public interface MatchMessageBeanLocal {
    
    public void notifySuscribers(MatchDTO matchDTO);
    
}

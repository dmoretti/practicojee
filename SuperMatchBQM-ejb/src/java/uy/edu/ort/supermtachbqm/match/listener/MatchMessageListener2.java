package uy.edu.ort.supermtachbqm.match.listener;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 *
 * @author damianmoretti
 */

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/Topic"),
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "jms/Topic"),
    @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "jms/Topic")
})
public class MatchMessageListener2 implements MessageListener {
    
    public MatchMessageListener2() {
    }
    
    @Override
    public void onMessage(Message message) {
        
        try {
            System.out.println("LISTENER 2: " + ((TextMessage)message).getText());
        } catch (JMSException ex) {
            Logger.getLogger(MatchMessageListener2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uy.edu.ort.supermtachbqm.match.service;

import java.util.List;
import javax.ejb.Local;
import uy.edu.ort.supermtachbqm.dto.MatchDTO;
import uy.edu.ort.supermtachbqm.entity.Match;

@Local
public interface MatchServiceBeanLocal {
    
    public List<Match> all();
    public void add(MatchDTO matchDTO);
    
    
}

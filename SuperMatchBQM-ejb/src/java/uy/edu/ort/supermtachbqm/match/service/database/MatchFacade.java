package uy.edu.ort.supermtachbqm.match.service.database;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import uy.edu.ort.supermtachbqm.database.AbstractFacade;
import uy.edu.ort.supermtachbqm.entity.Match;

/**
 *
 * @author damianmoretti
 */
@Stateless
public class MatchFacade extends AbstractFacade<Match> implements MatchFacadeLocal {
    
    @PersistenceContext(unitName = "SuperMatchBQM-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MatchFacade() {
        super(Match.class);
    }
    
}

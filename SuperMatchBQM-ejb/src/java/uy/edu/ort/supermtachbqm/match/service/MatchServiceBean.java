package uy.edu.ort.supermtachbqm.match.service;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import uy.edu.ort.supermtachbqm.dto.MatchDTO;
import uy.edu.ort.supermtachbqm.entity.Match;
import uy.edu.ort.supermtachbqm.match.service.database.MatchFacadeLocal;

@Stateless
public class MatchServiceBean implements MatchServiceBeanLocal {
   
    @EJB
    private MatchFacadeLocal matchFacade;
    
    @EJB
    private MatchMessageBeanLocal  matchMessageBean;
    
    @Override
    public List<Match> all(){     
        return matchFacade.findAll();        
    }
    
    @Override
    public void add(MatchDTO matchDTO){     
        
        // Match creation
        matchFacade.create(new Match(matchDTO));   
        
        // Match notifications
        matchMessageBean.notifySuscribers(matchDTO);        
    }
    
}

package uy.edu.ort.supermtachbqm.dto;

/**
 *
 * @author damianmoretti
 */
public class MatchDTO {
    
    private String nmLocalTeam;
    private String nmVisitorTeam;

    public MatchDTO(String nmLocalTeam, String nmVisitorTeam) {
        this.nmLocalTeam = nmLocalTeam;
        this.nmVisitorTeam = nmVisitorTeam;
    }
    
    public String getNmLocalTeam() {
        return nmLocalTeam;
    }

    public void setNmLocalTeam(String nmLocalTeam) {
        this.nmLocalTeam = nmLocalTeam;
    }

    public String getNmVisitorTeam() {
        return nmVisitorTeam;
    }

    public void setNmVisitorTeam(String nmVisitorTeam) {
        this.nmVisitorTeam = nmVisitorTeam;
    }

    @Override
    public String toString() {
        return "MatchDTO{" + "nmLocalTeam=" + nmLocalTeam + ", nmVisitorTeam=" + nmVisitorTeam + '}';
    }
    
}

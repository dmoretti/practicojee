package uy.edu.ort.supermatch.match.service;

import com.google.gson.Gson;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import uy.edu.ort.supermtachbqm.dto.MatchDTO;
import uy.edu.ort.supermtachbqm.entity.Match;
import uy.edu.ort.supermtachbqm.match.service.MatchServiceBeanLocal;

@Path("match")
public class MatchResource {

    @Context
    private UriInfo context;
    
    @EJB 
    MatchServiceBeanLocal matchServiceBean;
    
    public MatchResource() {
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMatches() {
        
        // NOTA: Aqui tendría que ser MatchDTO. Implementar Alumnos
        List<Match> lstMatches = matchServiceBean.all();
        
        String response = new Gson().toJson(lstMatches);
        return Response.ok(response).build();
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{id}")
    public Response getMatch(@PathParam("id") Integer id) {       
        
       return Response.status(Response.Status.OK).build();         
    }
    
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response postMatch(@FormParam("nmLocal") String nmLocal,
                              @FormParam("nmVisitor") String nmVisitor) {
        
        MatchDTO matchDTO = new MatchDTO(nmLocal,nmVisitor);
        matchServiceBean.add(matchDTO);
        
        return Response.status(Response.Status.CREATED).build();
        
    }
    
}
